import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;

/**
 * Created by mr1penguin on 2/5/15.
 */
@WebServlet(name = "/pdfoutput", urlPatterns = "/pdfoutput")
public class PdfOutput extends HttpServlet {

    public ByteArrayOutputStream createPDF() throws IOException, COSVisitorException {

        PDDocument document;
        PDPage page;
        PDPageContentStream contentStream;
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        // Creating Document
        document = new PDDocument();

        // Creating Pages
        page = new PDPage(new PDRectangle(300,300));

        // Adding page to document
        document.addPage(page);
        
        //image
        InputStream in = new URL("https://ru.gravatar.com/userimage/57870153/3f22d5c43fc89f01b1d9b903b1a38360.jpg?size=300").openStream();
        PDJpeg img = new PDJpeg(document, in);

        // Next we start a new content stream which will "hold" the to be created content.
        contentStream = new PDPageContentStream(document, page);

        // Let's define the content stream
        contentStream.drawImage(img, 0, 0);

        // Let's close the content stream
        contentStream.close();

        page = new PDPage(new PDRectangle(300,300));

        // Adding page to document
        document.addPage(page);

        //image
        img = new PDJpeg(document, new URL("https://ru.gravatar.com/userimage/57870153/1e67de78a90b2f7d4bf854995f2b866f.jpg?size=300").openStream());

        contentStream = new PDPageContentStream(document, page);

        contentStream.drawImage(img, 0, 0);

        contentStream.close();

        // Finally Let's save the PDF
        document.save(output);
        document.close();

        return output;
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.addHeader("Content-Type", "application/force-download");
        response.addHeader("Content-Disposition", "attachment; filename=\"Hello World.pdf\"");
        try {
            response.getOutputStream().write(createPDF().toByteArray());
        } catch (COSVisitorException e) {
            e.printStackTrace();
        }
    }
}
