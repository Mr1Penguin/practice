<%--
  Created by IntelliJ IDEA.
  User: mr1penguin
  Date: 2/1/15
  Time: 12:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
    <head>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://openlayers.org/en/v3.2.0/css/ol.css" type="text/css">
        <script src="http://openlayers.org/en/v3.2.0/build/ol.js" type="text/javascript"></script>
        <title></title>
        <style>
            div.olControlAttribution { bottom:3px; }
            #mapdiv {
                width:500px;
                height:500px;
            }
        </style>
    </head>
    <body>
        <div class="page-header">
            <h1>Test index page</h1>(<a href="pdfoutput">pdf</a>)
        </div>
        <div id="mapdiv"></div> <br>
        <script type="text/javascript">
            var map = new ol.Map({
                target: 'mapdiv',
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM({layer: 'sat'})
                    })
                ],
                view: new ol.View({
                    center: ol.proj.transform([37.41, 8.82], 'EPSG:4326', 'EPSG:3857'),
                    zoom: 4
                })
            });
        </script>

    </body>
</html>
